// Подключаем все нужные библиотеки:
#include <LiquidCrystal.h> //  - подключаем библиотеку для работы с экраном
#include "DHT.h" //  - подключаем библиотеку для датчика температуры
#include <iarduino_RTC.h> // - подключаем библиотеку для работы с часами реального времени


// ----------------------------------------

// Определяем глобальные настроечные переменные
#define DEBUG_MODE 1 // Дебаг режим?
#define PRINT_ALL_TO_SERIAL 1 // Писать лог в серийный порт?
#define SERIAL_START Serial.begin(9600); // Инициализация серийного порта

// ----------------------------------------

iarduino_RTC time(RTC_DS1302, 21, 19, 20); // подключаем RTC модуль на базе чипа DS1302, указывая выводы Arduino подключённые к выводам модуля RST, CLK, DAT

// Датчики:
// - Датчики температуры и влажности DHT22
#define DHT_TYPE DHT22 // Тип датчика
DHT DHTtomatoTop(22, DHT_TYPE); // Первый датчик - ПОМИДОР ВЕРХ
  float tomatoTempTop;  float tomatoHumidityAirTop; // Переменные для хранения данных о температуре и влажности воздуха от датчика 1
DHT DHTtomatoBottom(23, DHT_TYPE); // Второй датчик - ПОМИДОР НИЗ
  float tomatoTempBottom; float tomatoHumidityAirBottom; // Переменные для хранения данных о температуре и влажности воздуха от датчика 2
DHT DHTtomatoGround(24, DHT_TYPE); // Третий датчик - ПОМИДОМ ЗЕМЛЯ
  float tomatoTempGround; float tomatoHumidityAirGround; // Переменные для хранения данных о температуре и влажности воздуха от датчика 3
DHT DHTcucumberTop(25, DHT_TYPE); // Четвёртый датчик - ОГУРЕЦ ВЕРХ
  float cucumberTempTop;  float cucumberHumidityAirTop; // Переменные для хранения данных о температуре и влажности воздуха от датчика 4
DHT DHTcucumberBottom(26, DHT_TYPE); // Пятый датчик - ОГУРЕЦ НИЗ 
  float cucumberTempBottom; float cucumberHumidityAirBottom; // Переменные для хранения данных о температуре и влажности воздуха от датчика 5
DHT DHTcucumberGround(27, DHT_TYPE); // Шестой датчик - ОГУРЕЦ ЗЕМЛЯ
  float cucumberTempGround; float cucumberHumidityAirGround; // Переменные для хранения данных о температуре и влажности воздуха от датчика 6
// Для инициализации датчиков температуры
#define DHT_START DHTtomatoTop.begin(); DHTtomatoBottom.begin(); DHTtomatoGround.begin(); DHTcucumberTop.begin(); DHTcucumberBottom.begin(); DHTcucumberGround.begin();

// Датчик влажности почвы
#define PIN_HUMIDITY_SOIL_TOMATO_SENSOR A10 // PIN для первого аналогового датчика влажности почвы (помидор)
  int humiditySoilTomato; // Переменная для хранения данных о влажности почвы (помидор)
#define PIN_HUMIDITY_SOIL_CUCUMBER_SENSOR A11 // PIN для второго аналогового датчика влажности почвы (огурец)
  int humiditySoilCucumber; // Переменная для хранения данных о влажности почвы (огурец)

// Датчик освещённости
#define PIN_PHOTO_SENSOR A15 // PIN для аналогового датчика освещённости
int light_now; // Переменная для хранения текущего уровня освещённости


// Реле (выходы)
#define OUT_1_PIN 35 // Открыть окна помидоры
  boolean OUT_1_status = false;
  #define OUT_1_ON digitalWrite(OUT_1_PIN, HIGH); OUT_1_status = true;
  #define OUT_1_OFF digitalWrite(OUT_1_PIN, LOW); OUT_1_status = false;
#define OUT_2_PIN 36 // Открыть окна огурцы
  boolean OUT_2_status = false;
  #define OUT_2_ON digitalWrite(OUT_2_PIN, HIGH); OUT_2_status = true;
  #define OUT_2_OFF digitalWrite(OUT_2_PIN, LOW); OUT_2_status = false;
#define OUT_3_PIN 37 // Закрыть окна помидоры
  boolean OUT_3_status = false;
  #define OUT_3_ON digitalWrite(OUT_3_PIN, HIGH); OUT_3_status = true;
  #define OUT_3_OFF digitalWrite(OUT_3_PIN, LOW); OUT_3_status = false;
#define OUT_4_PIN 38 // Закрыть окна огурцы
  boolean OUT_4_status = false;
  #define OUT_4_ON digitalWrite(OUT_4_PIN, HIGH); OUT_4_status = true;
  #define OUT_4_OFF digitalWrite(OUT_4_PIN, LOW); OUT_4_status = false;
#define OUT_5_PIN 39 // Подогрев почвы помидор
  boolean OUT_5_status = false;
  #define OUT_5_ON digitalWrite(OUT_5_PIN, HIGH); OUT_5_status = true;
  #define OUT_5_OFF digitalWrite(OUT_5_PIN, LOW); OUT_5_status = false;
#define OUT_6_PIN 40 // Подогрев почвы огурцы
  boolean OUT_6_status = false;
  #define OUT_6_ON digitalWrite(OUT_6_PIN, HIGH); OUT_6_status = true;
  #define OUT_6_OFF digitalWrite(OUT_6_PIN, LOW); OUT_6_status = false;
#define OUT_7_PIN 41 // Подсветка помидоры (LED 1-6)
  boolean OUT_7_status = false;
  #define OUT_7_ON digitalWrite(OUT_7_PIN, HIGH); OUT_7_status = true;
  #define OUT_7_OFF digitalWrite(OUT_7_PIN, LOW); OUT_7_status = false;
#define OUT_8_PIN 42 // Подсветка огурцы (LED 7-9)
  boolean OUT_8_status = false;
  #define OUT_8_ON digitalWrite(OUT_8_PIN, HIGH); OUT_8_status = true;
  #define OUT_8_OFF digitalWrite(OUT_8_PIN, LOW); OUT_8_status = false;

#define M_1_PIN 43 // Вентилятор помидоры
  boolean M_1_status = false;
  #define M_1_ON digitalWrite(M_1_PIN, HIGH); M_1_status = true;
  #define M_1_OFF digitalWrite(M_1_PIN, LOW); M_1_status = false;
#define M_2_PIN 44 // Вентилятор огурцы
  boolean M_2_status = false;
  #define M_2_ON digitalWrite(M_2_PIN, HIGH); M_2_status = true;
  #define M_2_OFF digitalWrite(M_2_PIN, LOW); M_2_status = false;
#define M_3_PIN 45 // Полив помидоры
  boolean M_3_status = false;
  int M_3_startTime = 0;
  #define M_3_ON digitalWrite(M_3_PIN, HIGH); M_3_status = true;
  #define M_3_OFF digitalWrite(M_3_PIN, LOW); M_3_status = false;
#define M_4_PIN 46 // Полив огурцы
  boolean M_4_status = false;
  int M_4_startTimes = 0;
  #define M_4_ON digitalWrite(M_4_PIN, HIGH); M_4_status = true;
  #define M_4_OFF digitalWrite(M_4_PIN, LOW); M_4_status = false;




// Инициализация контактов экрана:
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
#define LCD_START lcd.begin(16, 2);

// Клавиатура:
#define DIRECTION_BUTTON_PIN 0 // PIN клавиатуры
int clickButton; // переменная для хранения текущего состояния нажатия
#define FREEZE_BUTTON 200 // На сколько морозим клавиатуру после нажатия клавиши (шагов)
int freezeButton = 0; // Текущее состояние заморозки (0 - ожидаем нажатие клавиши, >0 - клавиатура заморожена на мс)
int actionButton; // Условный код нажатой клавиши



// Переменные для интервальности просмотра:
int lastCheck = 0; // время прошлого считывания

#define ONE_STEP_DELAY 2 // задержка одного шага, мс

int nowTime = 0; // Переменная для хранения текущего времени (в секундах от начала дня)

void setup() {
  LCD_START // Старт экрана
  DHT_START // Старт датчиков температуры
  SERIAL_START // Старт серийного порта
  time.begin(); // Старт датчика реального времени
  
  pinMode(M_1_PIN, OUTPUT);
  pinMode(M_2_PIN, OUTPUT);
  pinMode(M_3_PIN, OUTPUT);
  pinMode(M_4_PIN, OUTPUT);

  pinMode(OUT_1_PIN, OUTPUT);
  pinMode(OUT_2_PIN, OUTPUT);
  pinMode(OUT_3_PIN, OUTPUT);
  pinMode(OUT_4_PIN, OUTPUT);
  pinMode(OUT_5_PIN, OUTPUT);
  pinMode(OUT_6_PIN, OUTPUT);
  pinMode(OUT_7_PIN, OUTPUT);
  pinMode(OUT_8_PIN, OUTPUT);

  delay(interval);
}

void loop() {
  // Считываем текущее время
  time.gettime(); nowTime = time.Hours * 3600 + time.minutes * 60 + time.seconds; // секунды с начала дня
  
  read_keyboard(); // Читаем клавиатуру

  // Если пришло время - нужно...
  if (nowTime != lastCheck) {
    read_sensor(); // ... проверить все дантчики....
    check_rules(); // ... отработать все правила...
    lastCheck = nowTime;  // ... обновить время последнего считывания датчиков...
    write_LCD(); // ... обновить данные на дисплее

    #ifdef PRINT_ALL_TO_SERIAL
      // если нужно писать в серийный порт данные - пишем их.
      printAllToSerial();
    #endif
  }
  

  #ifdef DEBUG_MODE
    // Отрисовываем текущий статус нажатия клавиши, работает только в дебаг режиме
    lcd.setCursor(15,0);
    switch(clickButton) {
      case 1023: lcd.print(" "); break;
      case 99: lcd.print("^"); break;
      case 256: lcd.print("v"); break;
      case 411: lcd.print("<"); break;
      case 0: lcd.print(">"); break;
      case 640: lcd.print("E"); break;
    }
  #endif  


delay(ONE_STEP_DELAY);
}


