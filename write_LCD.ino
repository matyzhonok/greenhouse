void write_LCD() {
    lcd.clear(); // Чистим экран....
    lcd.home(); // Переводим курсор на начальную позицию
    lcd.print(tomatoTempTop); lcd.print("*C "); lcd.print(tomatoHumidityAirTop); lcd.print("%"); // Пишем температуру и влажность воздуха на помидорах
    lcd.setCursor(0,1);     // ... переводим курсор на новую строку ...
    lcd.print(tomatoTempBottom); lcd.print("*C "); lcd.print(tomatoHumidityAirBottom); lcd.print("%"); // Пишем температуру и влажность воздуха на огурцах
}

