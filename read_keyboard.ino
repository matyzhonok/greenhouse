void read_keyboard(){
  // Проверяем, нужно ли считывать нажатую клавишу
  if(freezeButton<=1){
    // Считывать нужно....
    clickButton = analogRead(DIRECTION_BUTTON_PIN); // ... считываем нажатую клавишу ...
    switch(clickButton) {
      // ... переводим её в условный код нажатой клавиши ....
      case 99: actionButton = 1; freezeButton = FREEZE_BUTTON; break; // 1 = ВВЕРХ
      case 256: actionButton = 2; freezeButton = FREEZE_BUTTON; break; // 2 = ВНИЗ
      case 411: actionButton = 3; freezeButton = FREEZE_BUTTON; break; // 3 = ЛЕВО
      case 0: actionButton = 4; freezeButton = FREEZE_BUTTON; break; // 4 = ПРАВО
      case 640: actionButton = 5; freezeButton = FREEZE_BUTTON; break; // 5 = ВЫБОР
      default: actionButton = 0;
    }
    freezeButton = FREEZE_BUTTON; // .. и замораживаем клавиатуру на заданное количество шагов.
  } else{
    // Считывать не нужно ...
      freezeButton --; // ... уменьшаем счётчик шагов заморозки на 1 шаг.
  }
}
