void read_sensor() {
  // Снимаем данные с датчиков температуры и влажности:
  // Температура помидор
  tomatoTempTop = DHTtomatoTop.readTemperature(); // t1
  tomatoTempBottom = DHTtomatoBottom.readTemperature(); // t2
  tomatoTempGround = DHTtomatoGround.readTemperature(); // t3
  // Температура огурцов
  cucumberTempTop = DHTcucumberTop.readTemperature(); // t4
  cucumberTempBottom = DHTcucumberBottom.readTemperature(); // t5
  cucumberTempGround = DHTcucumberGround.readTemperature(); // t6
  // Влажность воздуха помидор
  tomatoHumidityAirTop = DHTtomatoTop.readHumidity();
  tomatoHumidityAirBottom = DHTtomatoBottom.readHumidity();
  tomatoHumidityAirGround = DHTtomatoGround.readHumidity();
  // Влажность воздуха огурцов
  cucumberHumidityAirTop = DHTcucumberTop.readHumidity();
  cucumberHumidityAirBottom = DHTcucumberBottom.readHumidity();
  cucumberHumidityAirGround = DHTcucumberGround.readHumidity();
  // освещённость
  light_now = analogRead(PIN_PHOTO_SENSOR);
  // Влажность почвы помидоров
  humiditySoilTomato = analogRead(PIN_HUMIDITY_SOIL_TOMATO_SENSOR); // H1
  // Влажность почвы огурцов
  humiditySoilCucumber = analogRead(PIN_HUMIDITY_SOIL_CUCUMBER_SENSOR); //H2
}
