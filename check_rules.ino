// Функция проверки всех условий включения/выключения

void check_rules() {
  // Rule 1: Если H1 < 30% (400 у.е.) - включить полив M3 на 10 секунд
  if (M_3_status) { // Проверяем, работает ли полив
    if (abs(nowTime - M_3_startTime) > 10) { // Если работает и прошло уже 10 секунд - выключаем
      M_3_OFF
    }
  } else{
    if (humiditySoilTomato < 400){ // Если не работает и влажность меньше заданной - включаем полив...
      M_3_ON
      M_3_startTime = nowTime; // ... и обновляем время включения
    }
  }

  // Rule 2: Если H2 < 20% (300 у.е.) - включить полив M4 на 10 секунд
    if (M_4_status) { // Проверяем, работает ли полив
    if (abs(nowTime - M_4_startTime) > 10) { // Если работает и прошло уже 10 секунд - выключаем
      M_4_OFF
    }
  } else{
    if (humiditySoilCucumber < 300){ // Если не работает и влажность меньше заданной - включаем полив...
      M_4_ON
      M_4_startTime = nowTime; // ... и обновляем время включения
    }
  }
  
  // Rule 3: t1 - t2 > 3 - включить вентиляторы M1 и M2, иначе - выключить.
  if ((tomatoTempTop - tomatoTempBottom) > 3){
    M_1_ON M_2_ON
  } else {
    M_1_OFF M_2_OFF
  }

  // Rule 4: t2 > 30 - включить вентиляторы M1 и M2, иначе - выключить.
  if (tomatoTempBottom > 30){
    M_1_ON M_2_ON
  } else {
    M_1_OFF M_2_OFF
  }
  
  // Rule 5: t1 > 30 - открыть окно помидор, иначе - закрыть
  if (tomatoTempTop > 30){
    OUT_1_ON
  } else {
    OUT_1_OFF
  }

  // Rule 6: Если t4 > 30 - включить OUT_3 - открыть окна огурцы
  if (cucumberTempTop > 30){
    OUT_3_ON
  } else {
    OUT_3_OFF
  }
  
  // Rule 7: Если t2 < 18 - включить OUT_2 - закрыть окна помидоры
  if (tomatoTempBottom < 18){
    OUT_2_ON
  } else {
    OUT_2_OFF
  }

  // Rule 8: Если t5 < 18 - включить OUT_4 - закрыть окна огурцы
  if (cucumberTempBottom < 18){
    OUT_4_ON
  } else {
    OUT_4_OFF
  }

  // Rule 9: Если t3 < 12 - включить OUT_5 - включить подогрев почвы помидоров
  if (tomatoTempGround < 12){
    OUT_5_ON
  } else {
    OUT_5_OFF
  }

  // Rule 10: Если t6 < 12 - включить OUT_6 - включить подогрев почвы огурцов
  if (cucumberTempGround < 12){
    OUT_6_ON
  } else {
    OUT_6_OFF
  }

  // Rule 11: Если Light < 1000 лм (900 у.е.) AND Time (8:00 - 20:00) - включить OUT_7 - включить подсветку помидор
  if ((light_now < 900) AND (time.Hours >= 8 AND time.Hours<20)) {
    OUT_7_ON
  } else {
    OUT_7_OFF
  }

  // Rule 12: Если Light < 1000 лм (900 у.е.) AND Time (8:00 - 20:00) - включить OUT_8 - включить подсветку огурцов
  if ((light_now < 900) AND (time.Hours >= 8 AND time.Hours<20)) {
    OUT_8_ON
  } else {
    OUT_8_OFF
  }
}

